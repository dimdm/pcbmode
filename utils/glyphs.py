#!/usr/bin/python

#   Copyright 2013 Boldport Limited
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import os
import re
import HTMLParser # required for HTML to unicode translation
from lxml import etree as et

# pcbmode modules
import svg
import utils
from Point import Point


def get_glyphs(cfg, text, font_name, location=Point(), scale=1, line_number=1, rotate=0):
    """
    Returns an array of paths that correspond to the 'text' string
    and initial 'location'
    """

    # open font SVG
    try: 
        font_data = et.ElementTree(file=os.path.join(cfg['base_dir'], cfg['pcbmode']['locations']['fonts'], font_name + '.svg')) 
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

    # check and create data structure for storing processed glyphs
    if cfg.get('fonts') is None:
        cfg['fonts'] = {}
    if cfg['fonts'].get(font_name) is None:
        cfg['fonts'][font_name] = {}

    # get dimensions of font
    # TODO: check how this is spec'd variable-width fonts
    font_horiz_adv_x = float(font_data.find("//n:font", namespaces={'n': cfg['namespace']['svg']}).get('horiz-adv-x'))
    glyph_height = float(font_data.find("//n:font-face", namespaces={'n': cfg['namespace']['svg']}).get('units-per-em'))
    glyph_ascent = float(font_data.find("//n:font-face", namespaces={'n': cfg['namespace']['svg']}).get('ascent'))
    glyph_decent = float(font_data.find("//n:font-face", namespaces={'n': cfg['namespace']['svg']}).get('descent'))

    text_height = glyph_height*scale
    text_width = 0

    paths = []

    rotate = 180-rotate

    place = Point(location.x, location.y + glyph_ascent/3*scale)

    offset_x = 0

    # split text into charcters (also find unicade chars)
    text = re.findall(r'(\&#x[0-9abcdef]*;|.)', text)

    # instantiate HTML parser
    htmlpar = HTMLParser.HTMLParser()

    for i, symbol in enumerate(text[:]):

        symbol = htmlpar.unescape(symbol)

        # get the glyph definition from the file
        glyph = font_data.find(u'//n:glyph[@unicode="%s"]' % symbol, namespaces={'n': cfg['namespace']['svg']})

        
        if glyph == None:
            print "ERROR: glyph not found for symbol %s" % symbol 
        else:

            # unless the glyph has its own width, use the global font width
            glyph_width = float(glyph.get('horiz-adv-x') or font_horiz_adv_x)

            # add glyph width to total
            text_width += glyph_width*scale

            # the division by '3' here is a possible hack that works for now...
            # TODO: figure out a consistent way to align text vertically, and how to properly do it
            #place = Point(i*glyph_width*scale+location.x, location.y+glyph_ascent/3*scale)
            place.x += glyph_width*scale
            #place.rotate(-rotate, location)

            # spaces won't have paths, so skip the following
            if symbol != ' ':

                # check if this glyph has already been processed
                glyph_path = cfg['fonts'][font_name].get(symbol)

                if glyph_path is None:
                    # get the path of the glyph
                    glyph_path = glyph.get('d')
                    # check to see if there's a gerber 'level polarity' ('LP') directive
                    glyph_path = svg.mirror_path_over_axis(glyph_path, 'x', glyph_width)
                    # convert to a relative path 
                    glyph_path = svg.absolute_to_relative_path(glyph_path)
                    cfg['fonts'][font_name][symbol] = glyph_path


                gerber_lp = glyph.get('gerber_lp') or ''

                # rotate and scale path
                fd_w, fd_h, glyph_path = svg.transform_path(glyph_path, False, scale, rotate)
                
                line_extra = glyph_height*scale*(line_number - 1)

                paths.append({"d": glyph_path, 
                              "symbol": symbol, 
                              "location": [place.x, place.y - line_extra], 
                              "gerber_lp": gerber_lp})
            


    return paths, text_width, text_height
