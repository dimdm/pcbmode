#!/usr/bin/python

#   Copyright 2013 Boldport Limited
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import json
import os
import re
import zipfile
import subprocess as subp # for shell commands

# pcbmode modules
from Point import Point



def dict_to_style(style_dict):
    """
    convert a dictionary into an SVG/CSS style attribute
    """

    style = None

    if style_dict is not None:
        style = ''
        for key in style_dict:
            style += key+':'+style_dict[key]+';'

    return style






def to_Point(coord=[0, 0]):
    """
    Takes a coordinate in the form of [x,y] and
    returns a Point type
    """
    return Point(coord[0], coord[1])





def get_git_revision():

    rev = 'unknown '
    command = ['git', 'describe', '--tags', '--long']
    try:
        rev = subp.check_output(command)
    except:
        pass

    return rev[:-1]






def create_pngs(cfg):
    """
    Prepares all files necessary for producing the PCB
    """

    # directory for storing the Gerbers within the build path
    images_path = os.path.join(cfg['base_dir'], 
                               cfg['pcbmode']['locations']['build'], 
                               'images')
    create_dir(images_path)


    # create individual PNG files for layers
    png_dpi = 600
    print "-- generating PNGs for each layer of the board"
    layers = ['copper_layer', 'silkscreen_layer', 'soldermask_layer', 
              'outline_layer', 'assembly_layer', 'documentation_layer']

    command = ['inkscape', 
               '--without-gui', 
               '--file=%s' % os.path.join(cfg['base_dir'], 
                                          cfg['pcbmode']['locations']['build'], 
                                          cfg['board_name'] + '.svg'), 
               '--export-png=%s' % os.path.join(images_path, cfg['board_name'] + '.png'), 
               '--export-dpi=%s' % str(png_dpi), 
               '--export-area-drawing',
               '--export-background=#FFFFFF']

    try:
        subp.call(command)
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            print "WARNING: Inkscape is needed for converting SVG to PNG; is it installed?"
            print "         The program will continue without generating PNGs, then."
        else:
            print "ERROR: can't run Inkscape for some reason"
            raise

    for layer in get_surface_layers(cfg):
        command = ['inkscape', 
                   '--without-gui', 
                   '--file=%s' % os.path.join(cfg['base_dir'], 
                                              cfg['pcbmode']['locations']['build'], 
                                              cfg['board_name'] + '.svg'), 
                   '--export-png=%s' % os.path.join(images_path, cfg['board_name'] + '_rev_' + 
                                                    cfg['board']['meta']['board_revision'] 
                                                    + '_' + layer + '.png'),
                   '--export-dpi=%s' % str(png_dpi),
                   '--export-id-only',
                   '--export-area-drawing',
                   '--export-background=#FFFFFF',
                   '--export-id=%s' % layer]
        
        try:
            subp.call(command)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print "WARNING: Inkscape is needed for converting SVG to PNG; is it installed?"
                print "         The program will continue without generating PNGs, then."
            else:
                print "ERROR: can't run Inkscape for some reason"
                raise

    return




def get_json_data_from_file(filename):
    """
    Open a json file and returns its content as a dict
    """

    json_data = None

    try:
        data = open(filename, 'rb')
        json_data = json.load(data)
        data.close()
    except IOError, OSError:
        print "WARNING: couldn't open JSON file: %s", filename
        pass

    return json_data





def get_surface_layers(cfg):
    """
    Returns a list of sorface layer names
    """    

    board_stackup = cfg['board']['physical'].get('stackup')
    if board_stackup is not None:
        if len(board_stackup) == 1:
            surface_layers = ['top']
        elif len(board_stackup) == 2:
            surface_layers = ['top', 'bottom']
        else:
            print "ERROR: PCBmodE currently only supports two layers"
            raise

    return surface_layers





def create_dir(path):
    """
    Checks if a directory exists, and creates one if not
    """
    
    try:
        # try to create directory first; this prevents TOCTTOU-type race condition
        os.makedirs(path)
    except OSError:
        # if the dir exists, pass
        if os.path.isdir(path):
            pass
        else:
            print "ERROR: couldn't create build path %s" % path
            raise

    return





def add_dict_values(d1, d2):
    """
    Add the values of two dicts
    Helpful code here:
      http://stackoverflow.com/questions/1031199/adding-dictionaries-in-python
    """

    return dict((n, d1.get(n, 0)+d2.get(n, 0)) for n in set(d1)|set(d2) )





def process_meander_type(type_string):
    """
    Extract meander path type parameters and return them as a dict
    """


    print type_string

    look_for = ['radius', 'theta', 'width', 'number', 'pitch']

    meander = {}

    for param in look_for:
        tmp = re.search('\s*%s\s*=\s*(?P<v>[^;]*)' % param, type_string)
        if tmp is not None:
            meander[param] = float(tmp.group('v'))

    # add optional fields as 'None'
    for param in look_for:    
        if meander.get(param) is None:
            meander[param] = None

    return meander



