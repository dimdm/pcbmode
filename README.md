
# PCBmodE 
## A PCB design tool written in Python around json, SVG, and Inkscape

### Introduction

PCBmodE reads shape and placement information stored in json files to produce an SVG graphical representation of them. Routing is drawn with [Inkscape](http://www.inkscape.org), then extracted by PCBmodE and stored in an input json file that's used for the next board generation. A post-processor 'gerberises' the SVGs into "Extended [Gerbers](http://en.wikipedia.org/wiki/Gerber_format)" (RS-274X) for manufacturing. PCBmodE uses the Inkscape XML namespace for layer information so that when viewed with Inkscape individual layers (copper, soldermask, silkscreen, etc.) can be shown or hidden. If no layer information is needed, any SVG viewer, such as a browser, can show the board.

### Motivation

PCBmodE was motivated by:

* the desire to _easily_ create arbitrary shapes on all PCB layers;
* the need for version controlled primary source files written in open, free, and standardised formats;
* the frustration with purpose-build GUI particularities, and the inability to _easily_ define feature shapes and location in a precise manner; and finally, 
* my desire to create functional and artistic PCBs, not possible with any other software.

In the long term, PCBmodE will hopefully motivate:

* an open, comprehensive, community-based repository of electronic components; and
* a netlist-based design tool, from which both layout and schematics are generated (in contrast to existing development flows).

### Status

The project is in its very early stages, and _won't yet be practically useful to most_. As a proof-of-concept, PCBmodE highlights where it and other EDA tools can go using open and modern tools and development methodologies. 

The goal for v1.0 was to be able to send a board for manufacturing. For that reason, the code wasn't written for performance, and is not the prettiest. These aspects of the project will improve as the software is packaged properly and the structure solidifies. Documentation is practically non-existent, and will be added as the software matures. 

### Name

The 'mod' in PCBmodE has a double meaning. The first reflects my vision for PCBmodE to be 'modern' tool, in contrast to the tired ol' EDA tools we have today; the second is a play on the familiar 'modifications' or 'mods' we make to PCBs. The original codename was 'PCBmod', but that was taken, so I changed it to PCBmodE. Call it 'PCB mode' or 'PCB mod E', whichever you prefer. 

### Usage

For a list of available commands, use 

    python pcbmode.py --help

There's a 'getting started' guide on the [wiki](https://bitbucket.org/boldport/pcbmode/wiki/Home).

### Contact

Contact [saardrimer@gmail.com](mailto:saardrimer@gmail.com) or get updates at [http://boldport.blogspot.co.uk](http://boldport.blogspot.co.uk) and [@boldport](http://www.twitter.com/boldport) and Boldport's [discussion group](https://groups.google.com/forum/#!forum/boldport).

### License

PCBmodE is distributed under the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
